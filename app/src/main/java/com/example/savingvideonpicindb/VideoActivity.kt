package com.example.savingvideonpicindb

import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Environment.DIRECTORY_MOVIES
import android.os.Bundle
import android.os.Environment
import android.widget.ImageView
import android.widget.VideoView
import java.io.*

class VideoActivity : AppCompatActivity() {
    val db = MyDBHelper(this,null)
    val pathFolder = File(Environment.getExternalStoragePublicDirectory(DIRECTORY_MOVIES).path)
//    override fun onBackPressed() {
//        startActivity(Intent(this@VideoActivity, MainActivity::class.java))
//        finish()
//    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 1 && null != data && resultCode == RESULT_OK){
            val uri = Uri.parse(data.data!!.toString())
            var video = File(uri.path)
            var vidbytearray = contentResolver.openInputStream(uri)!!.readBytes()
            var form = video.name.substringAfter(".")
            db.adVid(vidbytearray,form)
            val dbVid = db.getVid()
            val videoView = findViewById<VideoView>(R.id.videoView)
            WriteToDiskFile(pathFolder,dbVid, "title.mp4", object : OnAnswerWriteFile{
                override fun onWrite(file: File?) {
                    if(file != null && file.exists()){
                        videoView.setVideoURI(Uri.fromFile(file))
                    }
                }
            }).execute()
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)
        var intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.setType("video/*")
        startActivityForResult(intent, 1)
    }
}

class WriteToDiskFile(private val pathFolder: File, private val vidByte : ByteArray, private val title: String, private val onAnswerWriteFile: OnAnswerWriteFile): AsyncTask<Void, Void, File?>() {
    override fun doInBackground(vararg p0: Void?): File? {
        val vidFile = writeToDisk(title, vidByte)
        return vidFile
    }

    override fun onPostExecute(result: File?) {
        onAnswerWriteFile.onWrite(result)
    }

    private fun writeToDisk(title: String, vidByte: ByteArray): File? {
        try {
            val newFile = File(pathFolder.absolutePath, title)
            var inputStream: InputStream? = null
            var outputStream: OutputStream? = null
            try {
                val fileReader = ByteArray(4096)
                inputStream = vidByte.inputStream()
                try {
                    outputStream = FileOutputStream(newFile)
                } catch (e: java.lang.Exception) {
                    return null
                }
                while (true) {
                    val read = inputStream.read(fileReader)
                    if (read == -1)
                        break
                    outputStream.write(fileReader, 0, read)
                }
                outputStream.flush()
                return newFile
            } catch (e: IOException) {
                return null
            } finally {
                inputStream?.close()
                outputStream?.close()
            }
        } catch (e: IOException) {
            return null
        }


    }
}