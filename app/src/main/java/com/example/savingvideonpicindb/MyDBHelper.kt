package com.example.savingvideonpicindb

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class MyDBHelper(context: Context, factory: SQLiteDatabase.CursorFactory?): SQLiteOpenHelper(context,
    DATABASE_NAME, factory, DATABASE_VERSION)
{
    companion object{
        private val DATABASE_NAME = "DB4"
        private val DATABASE_VERSION = 1
        val TABLE_NAME = "MyTable"
    }

    override fun onCreate(p0: SQLiteDatabase?) {
        val query = ("CREATE TABLE $TABLE_NAME (id integer PRIMARY KEY, arrbyte varbinary(6000000) not null, format varchar(500) not null)")
        p0?.execSQL(query)
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        p0?.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(p0)
    }
    fun adPic(pic: ByteArray, format: String){
        val values = ContentValues()
        values.put("arrbyte", pic)
        values.put("format", format)
        val db = this.writableDatabase
        db.insert(TABLE_NAME,null,values)
        db.close()
    }
    fun getPic(): ByteArray{
        val db = this.readableDatabase
        var picbyte: ByteArray = byteArrayOf()
        var format = " "
        val result = db.rawQuery("SELECT * FROM $TABLE_NAME ORDER BY id DESC LIMIT 1" , null)
        if (result.moveToLast()){
            picbyte = result.getBlob(1)
            format = result.getString(2)
        }
        return picbyte
    }
    fun adVid(vid: ByteArray, format: String){
        val values = ContentValues()
        values.put("arrbyte", vid)
        values.put("format", format)
        val db = this.writableDatabase
        db.insert(TABLE_NAME,null,values)
        db.close()
    }
    fun getVid(): ByteArray{
        val db = this.readableDatabase
        var vidbyte: ByteArray = byteArrayOf()
        var format = " "
        val result = db.rawQuery("SELECT * FROM $TABLE_NAME ORDER BY id DESC LIMIT 1" , null)
        if (result.moveToLast()){
            vidbyte = result.getBlob(1)
            format = result.getString(2)
        }
        return vidbyte
    }
}
