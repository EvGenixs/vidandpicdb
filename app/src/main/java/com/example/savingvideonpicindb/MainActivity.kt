package com.example.savingvideonpicindb

import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import java.io.File

class MainActivity : AppCompatActivity() {
    val db = MyDBHelper(this,null)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 1 && null != data && resultCode == RESULT_OK){
            val uri = Uri.parse(data.data!!.toString())
            var pic = File(uri.path)
            var picbytearray = contentResolver.openInputStream(uri)!!.readBytes()
            var form = pic.name.substringAfter(".")
            db.adPic(picbytearray,form)
            val dbPic = db.getPic()
            var bitmap = BitmapFactory.decodeByteArray(dbPic,0,dbPic.size)
            val image = findViewById<ImageView>(R.id.imageView)
            image.setImageBitmap(bitmap)
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var but = findViewById<Button>(R.id.goToVidBut)
        but.setOnClickListener {
            startActivity(Intent(this@MainActivity,VideoActivity::class.java))
            finish()
        }
        var intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.setType("image/*")
        startActivityForResult(intent, 1)
    }
}