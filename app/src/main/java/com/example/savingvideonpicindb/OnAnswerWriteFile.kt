package com.example.savingvideonpicindb

import java.io.File

interface OnAnswerWriteFile {
    fun onWrite(file: File?)
}